import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: '', // e.g: 'Name | Developer'
  lang: '', // e.g: en, es, fr, jp
  description: '', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: 'Hello 🖐️ my name is',
  name: 'Kaci',
  subtitle: 'i am looking for an intership',
  cta: 'know more ',
};

// ABOUT DATA
export const aboutData = {
  img: 'IMG_20201108_120844_843.jpg',
  paragraphOne:
    'Master 2 Program and Safe Software student(Programmation Logiciels et Surs) at Sorbonne University Paris North. During my studies I acquired a solid foundation in software development, database manipulation, web development and continuous application integration and deployment .',
  paragraphTwo:
    'I am looking for a 6-month internship from March 2021 in software development, Devops engeniring , or Full Stack engenering .',
  paragraphThree:
    'This internship is essential to the validation of my degree and represents a potential experience that will allow me to realize and develop my skills .',
  resume: 'https://drive.google.com/file/d/12zDGLLgSoHV57h_hnrPBye8Y-utx4cMu/view?usp=sharing', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'Capture2.png',
    title: 'React Chat App',
    info: 'Developped with ReactJS, JavaScritp, HTML/CSS, Chat Engine',
    info2: 'To log in: username "user" password "123"',
    url: 'https://hedras.netlify.app/',
    repo: 'https://gitlab.com/belkadikaci/hedras', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'devops.png',
    title: 'DevOps',
    info: 'Implementation of continuous integration chain with Gitlab-ci and AMAZON EKS',
    info2: '',
    url: 'https://gitlab.com/belkadikaci/auto-devops/-/pipelines/270277169',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'facial_recognition.jpg',
    title: 'Face Recognition App',
    info: 'Developped with Python, OpenCV',
    info2: '',
    url: 'https://gitlab.com/belkadikaci/face_recognition/-/blob/master/README.md',
    repo: 'https://gitlab.com/belkadikaci/face_recognition', // if no repo, the button will not show up
  },

  {
    id: nanoid(),
    img: 'api.jpg',
    title: 'Web Api CRUD',
    info: ' Web API for CRUD operation to manage surveys',
    info2: 'Developped with JavaScritp, nodeJS, ExpressJs',
    url: 'https://github.com/belkadikaci/jsau-webapp',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'Capture1.PNG',
    title: 'Personal Portfolio',
    info: ' MY Portfolio Website',
    info2: 'Developped with ReactJS, JavaScritp, SCSS, Gatsby',
    url: 'https://kacisportfolio.netlify.app/',
    repo: '', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: 'If my profile interests you',
  btn: '',
  email: 'kacibelkadi21@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'instagram',
      url: 'https://www.instagram.com/kanybelkadi/',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/kaci-belkadi',
    },
    {
      id: nanoid(),
      name: 'gitlab',
      url: 'https://gitlab.com/users/belkadikaci/projects',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
